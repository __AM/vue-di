import Vue from 'vue';
import App from '@/test-app/app.vue';
import { DiFactory } from '@/lib/vue-di/di.factory';
import { CounterService } from '@/test-app/counter.service';
import { ClicksService } from '@/test-app/clicks.service';


Vue.config.productionTip = false;


const appDependencies = {
    $counterService: CounterService,
    $clicksService: ClicksService,
};
Vue.use(DiFactory, appDependencies);


new Vue({
    render: (h) => h(App),
}).$mount('#app');
