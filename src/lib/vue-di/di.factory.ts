import { readFunctionArgs } from './read-function-args';


type Injection = any;
interface InjectionsMap { [k: string]: Injection; }
interface FlagsMap { [k: string]: boolean; }


export class DiFactory {

    static install(vue: any, initProviders: any) {
        vue.mixin({
            beforeCreate() {
                if (!this.$di) {
                    const parentDi = this.$parent ? this.$parent.$di : null;
                    const ownProviders = this.$options.provide;
                    this.$di =
                        !parentDi
                            ? new DiFactory(this, Object.assign({}, initProviders, ownProviders || {}))
                            : (ownProviders
                                ? parentDi.copy(this, ownProviders)
                                : parentDi);
                }
            }
        });
    }


    protected deps!: any;


    constructor(component: any, injectionDataMap: InjectionsMap = {}) {
        this.deps = this.initInjections(component, Object.keys(injectionDataMap), injectionDataMap);
    }


    copy(component: any, newDeps: any = {}): DiFactory {
        const deps = Object.assign({}, this.deps, newDeps);
        return new DiFactory(component, deps);
    }


    protected initInjections(component: any, injectionNames: string[], allInjectionsMap: InjectionsMap, injectionsBeingInitialised: FlagsMap = {}, parentInjectionName = '') {
        injectionNames.forEach(injectionName => {
            const fn = allInjectionsMap[injectionName] || (injectionName[0] === '$' && component[injectionName]);
            if (!fn) {
                throw new Error(`Non existing dependency "${injectionName}" required by "${parentInjectionName}".`);
            }
            const statusNames = Object.keys(injectionsBeingInitialised);
            if (injectionsBeingInitialised[injectionName]) {
                throw new Error(`Dependency cycle detected: "${injectionName}" required by "${parentInjectionName}".`);
            }
            injectionsBeingInitialised[injectionName] = true;
            allInjectionsMap[injectionName] =
                fn.constructor.name === 'Function'
                    ? new fn(...this.getDependencies(fn, component, allInjectionsMap, injectionsBeingInitialised))
                    : fn;
            injectionsBeingInitialised[injectionName] = false;
        });
        return allInjectionsMap;
    }


    protected getDependencies(fn: any, component: any, injectionDataMap: InjectionsMap, injectionsBeingInitialised: FlagsMap) {
        const dependencyNames = readFunctionArgs(fn);
        this.initInjections(component, dependencyNames, injectionDataMap, injectionsBeingInitialised, fn.name);
        return dependencyNames.map((dependencyName: string) => injectionDataMap[dependencyName]);
    }
}
