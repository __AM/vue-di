import { readFunctionArgs } from './read-function-args';


describe('readFunctionArgs', () => {

    it('', () => {
        const expectedArgs = [
            '$dependencyA',
            '$dependencyB'
        ];
        expect(readFunctionArgs(testFunction_01)).toEqual(expectedArgs);
    });


    function testFunction_01(
        /* aa(
      aaa
      )aa$aa,a */
        $dependencyA: string,
        $dependencyB: number,
        /* b(bb)bb$bb,)bbb */
        ) {}
});
