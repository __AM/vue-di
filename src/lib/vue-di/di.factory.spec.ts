import { DiFactory } from './di.factory';
import { ClassA_WithDepToB } from '../__test-data/class-A-with-dep-to-B';
import { ClassB_WithDepToC } from '../__test-data/class-B-with-dep-to-C';
import { ClassC } from '../__test-data/class-C';
import { TestLogger } from '../__test-data/test-logger';
import { ClassC_WithDepToA } from '../__test-data/class-C-with-dep-to-A';
import { TestInjection } from '../__test-data/test-injection';
import { ClassC_WithDepToNotDefinedInjection } from '../__test-data/class-C-with-dep-to-not-defined-injection';
import { ClassC_WithRouter } from '../__test-data/class-C-with-router';


describe('DiFactory', () => {

    let vue: any;
    let $router: any = { nam: 'router' };
    let component: any;


    beforeEach(() => {
        vue = {
            mixin: jest.fn()
        };
        component = {
            $options: {},
            $router
        };
    });


    describe('static', () => {

        describe('install()', () => {

            let mainInjections: any;


            beforeEach(() => {
                mainInjections = {
                    testinjection: {}
                };
                DiFactory.install(vue, mainInjections);
            });


            it('should install mixins', () => {
                expect(vue.mixin.mock.calls.length).toBe(1);
            });


            describe('installed mixin', () => {

                let beforeCreateHook: any;


                beforeEach(() => {
                    beforeCreateHook = getBeforeCreateHook();
                });


                it('should set beforeCreate hook', () => {
                    expect(beforeCreateHook).toBeDefined();
                });


                describe('beforeCreateHook', () => {

                    it('should create dependency injector ($di) with main injections when component has none', () => {
                        beforeCreateHook.call(component);
                        expect(component.$di).toEqual(new DiFactory({}, mainInjections));
                    });


                    it('should create dependency injector ($di) with main & own injections when component has none', () => {
                        const componentInjections = {
                            ownInjection: {}
                        };
                        component.$options.provide = componentInjections;
                        beforeCreateHook.call(component);
                        const combinedInjections = Object.assign({}, mainInjections, componentInjections);
                        expect(component.$di).toEqual(new DiFactory({}, combinedInjections));
                    });


                    it('should not replace dependency injector ($di) when is already defiend', () => {
                        const expectedDi = {};
                        component.$di = expectedDi;
                        beforeCreateHook.call(component);
                        expect(component.$di).toBe(expectedDi);
                    });


                    describe('when parent component has $di defined', () => {

                        let parentDi: any;


                        beforeEach(() => {
                            parentDi = {
                                copy: jest.fn()
                            };
                            component.$parent = {
                                $di: parentDi,
                            };
                        });


                        it('should set parent component\'s dependency injector ($di) when present', () => {
                            beforeCreateHook.call(component);
                            expect(component.$di).toBe(parentDi);
                        });


                        it('should extend parent component\'s dependency injector ($di) with given new injections', () => {
                            const componentInjections = {
                                ownInjection: {}
                            };
                            const copiedDi = {};
                            parentDi.copy.mockReturnValueOnce(copiedDi);
                            component.$options.provide = componentInjections;
                            beforeCreateHook.call(component);
                            expect(parentDi.copy.mock.calls.length).toEqual(1);
                            expect(parentDi.copy.mock.calls[0]).toEqual([ component, componentInjections ]);
                            expect(component.$di).toEqual(copiedDi);
                        });
                    });
                });
            });
        });
    });


    describe('non-static', () => {

        let factory!: any;
        let mainInjections: any;


        beforeEach(() => {
            mainInjections = {
                injectionA: TestInjection,
                injectionB: TestInjection
            };
            factory = new DiFactory(mainInjections);
        });


        describe('copy()', () => {

            it('should set component\'s own injections upon main & parent\'s injections', () => {
                const componentInjections: any = {
                    injectionB: TestInjection,
                    injectionC: TestInjection
                };
                const copiedDi = factory.copy(component, componentInjections);
                expect(copiedDi.deps.injectionA).toEqual(factory.deps.injectionA);
                expect(copiedDi.deps.injectionB).not.toEqual(factory.deps.injectionB);
                expect(typeof copiedDi.deps.injectionB).not.toEqual('function');
                expect(copiedDi.deps.injectionB).toEqual(copiedDi.deps.injectionB);
                expect(copiedDi.deps.injectionC).toEqual(copiedDi.deps.injectionC);
            });
        });
    });


    describe('dependency initialisation', () => {

        let $logger!: any;
        let beforeCreateHook!: any;


        it('should instantiate nested, non-circular dependencies in correct order', () => {
            setupTest({
                $classC: ClassC,
            });
            expect(() => {
                beforeCreateHook.call(component);
            }).not.toThrowError();
            const callsArguments = $logger.log.mock.calls.map((args: string[]) => args[0]);
            expect(callsArguments).toEqual([
                'ClassC',
                'ClassB_WithDepToC',
                'ClassA_WithDepToB',
            ]);
        });


        it('should throw error when dependencies circle has been detected', () => {
            setupTest({
                $classC: ClassC_WithDepToA,
            });
            expect(() => {
                beforeCreateHook.call(component);
            }).toThrowError('Dependency cycle detected: "$classA" required by "ClassC_WithDepToA".');
        });


        it('should throw exception when required injection has not been specified', () => {
            setupTest({
                $classC: ClassC_WithDepToNotDefinedInjection,
            });
            expect(() => {
                beforeCreateHook.call(component);
            }).toThrowError('Non existing dependency "$notDefinedInjection" required by "ClassC_WithDepToNotDefinedInjection".');
        });


        it('should add $router to dependencies', () => {
            setupTest({
                $classC: ClassC_WithRouter,
                $options: { provide: { $router } }
            });
            beforeCreateHook.call(component);
            expect(component.$di.deps.$router).toBeDefined();
        });


        it('should replace main injection with one specified on the component level', () => {
            $logger = { log: jest.fn() };
            const $classC = new ClassC($logger);
            const $classB = new ClassB_WithDepToC($logger, $classC);
            const $classA = new ClassA_WithDepToB($logger, $classB);
            const injections = {
                $classA,
                $classB,
                $classC,
                $logger
            };
            const componentClassA = new ClassA_WithDepToB($logger, $classB);
            component = {
                $options: {
                    provide: {
                        $classA: componentClassA
                    }
                }
            };
            DiFactory.install(vue, injections);
            beforeCreateHook = getBeforeCreateHook();
            beforeCreateHook.call(component);
            const { deps } = component.$di;
            expect(deps.$classA.salt).toEqual(componentClassA.salt);
            expect(deps.$classB.salt).toEqual($classB.salt);
            expect(deps.$classC.salt).toEqual($classC.salt);
        });


        function setupTest({ $classC, $options = {} }: TestSetupData) {
            $logger = { log: jest.fn() };
            const injections = {
                $classA: ClassA_WithDepToB,
                $classB: ClassB_WithDepToC,
                $classC,
                $logger,
                $router
            };
            component = { $options };
            DiFactory.install(vue, injections);
            beforeCreateHook = getBeforeCreateHook();
        }
    });


    function getBeforeCreateHook() {
        return vue.mixin.mock.calls[0][0].beforeCreate;
    }
});


interface TestSetupData {
    $classC: any;
    $options?: any;
}
