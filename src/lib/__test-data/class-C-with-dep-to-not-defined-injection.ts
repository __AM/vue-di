import { TestLogger } from './test-logger';
import { NotDefinedInjection } from '@/lib/__test-data/not-defined-injection';


export class ClassC_WithDepToNotDefinedInjection {

    salt = Math.random();


    constructor(private $logger: TestLogger,
                private $notDefinedInjection: NotDefinedInjection) {
        $logger.log('ClassC_WithDepToNotDefinedInjection');
    }
}
