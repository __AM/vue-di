import { ClassC } from './class-C';
import { TestLogger } from './test-logger';


export class ClassB_WithDepToC {

    salt = Math.random();


    constructor(private $logger: TestLogger,
                private $classC: ClassC) {
        $logger.log('ClassB_WithDepToC');
    }
}
