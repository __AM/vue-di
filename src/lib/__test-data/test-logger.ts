export interface TestLogger {
    log(s: string): void;
}
