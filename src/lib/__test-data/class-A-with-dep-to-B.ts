import { ClassB_WithDepToC } from './class-B-with-dep-to-C';
import { TestLogger } from './test-logger';


export class ClassA_WithDepToB {

    salt = Math.random();


    constructor(private $logger: TestLogger,
                private $classB: ClassB_WithDepToC) {
        $logger.log('ClassA_WithDepToB');
    }
}
