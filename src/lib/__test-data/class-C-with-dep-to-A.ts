import { ClassA_WithDepToB } from './class-A-with-dep-to-B';
import { TestLogger } from './test-logger';


export class ClassC_WithDepToA {

    salt = Math.random();


    constructor(private $logger: TestLogger,
                private $classA: ClassA_WithDepToB) {
        $logger.log('ClassC_WithDepToA');
    }
}
