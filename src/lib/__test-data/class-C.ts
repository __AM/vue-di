import { TestLogger } from './test-logger';


export class ClassC {

    salt = Math.random();


    constructor(private $logger: TestLogger) {
        $logger.log('ClassC');
    }
}
