import { observable } from 'mobx';
import { ClicksService } from '@/test-app/clicks.service';


export class CounterService {

    @observable private _count = 0;


    constructor(private $clicksService: ClicksService) {}


    init() {}


    get count() {
        return this._count;
    }


    increase(): void {
        this._count++;
        this.$clicksService.increase();
    }
}
