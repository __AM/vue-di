import { observable } from 'mobx';


export class ClicksService {

    @observable private _count = 0;


    init() {}


    get count() {
        return this._count;
    }


    increase(): void {
        this._count++;
    }
}
