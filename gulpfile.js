const gulp = require('gulp');
const uglify = require('gulp-uglify');
const uglifyEs = require('gulp-uglify-es').default;
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const ts = require('gulp-typescript');
const tsConfig = require('./tsconfig.json').compilerOptions;
const merge = require('merge-stream');


const SOURCE_DIR = './src/lib/vue-di/';
const DIST_DIR = './dist/';
const TS_FILES = [ SOURCE_DIR + '**/*.ts', `!${SOURCE_DIR}**/*.spec.ts` ];


gulp.task('default', [ 'ts:compile' ]);


gulp.task('dist', [ 'ts:copy', 'dist:es6', 'dist:es5' ]);


gulp.task('dist:es6', [ 'ts:compile' ], () => {
    return distFn({
        target: 'es6',
        uglifyFn: uglifyEs
    });
});


gulp.task('dist:es5', [ 'ts:compile' ], () => {
    return distFn({
        target: 'es5',
        uglifyFn: uglify
    });
});


gulp.task('ts:copy', () => {
    return gulp.src(TS_FILES)
        .pipe(gulp.dest(DIST_DIR));
});


gulp.task('ts:compile', () => {
    const tsResult =
        gulp.src(TS_FILES)
            .pipe(plumber())
            .pipe(ts(tsConfig));
    return tsResult.dts.pipe(gulp.dest(DIST_DIR));
});


function distFn({ target, uglifyFn }) {
    const newTsConfig = Object.assign({}, tsConfig, { target });
    return gulp
        .src(TS_FILES)
        .pipe(plumber())
        // .pipe(sourcemaps.init())
        .pipe(ts(newTsConfig))
        // .pipe(uglifyFn())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(DIST_DIR + target + '/'));
}
