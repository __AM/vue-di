"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function readFunctionArgs(fn) {
    const matches = (fn + '')
        .replace(/\/\*([^\*]*)\*\//g, '')
        .match(/\(([^\)]*)\)/);
    return (!matches
        ? []
        : matches[1]
            .replace(/(\s|\n|\r)*/g, '')
            .split(',')
            .filter((item) => !!item));
}
exports.readFunctionArgs = readFunctionArgs;
