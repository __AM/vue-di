"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const read_function_args_1 = require("./read-function-args");
class DiFactory {
    static install(vue, initProviders) {
        vue.mixin({
            beforeCreate() {
                if (!this.$di) {
                    const parentDi = this.$parent ? this.$parent.$di : null;
                    const ownProviders = this.$options.provide;
                    this.$di =
                        !parentDi
                            ? new DiFactory(this, Object.assign({}, initProviders, ownProviders || {}))
                            : (ownProviders
                                ? parentDi.copy(this, ownProviders)
                                : parentDi);
                }
            }
        });
    }
    constructor(component, injectionDataMap = {}) {
        this.deps = this.initInjections(component, Object.keys(injectionDataMap), injectionDataMap);
    }
    copy(component, newDeps = {}) {
        const deps = Object.assign({}, this.deps, newDeps);
        return new DiFactory(component, deps);
    }
    initInjections(component, injectionNames, allInjectionsMap, injectionsBeingInitialised = {}, parentInjectionName = '') {
        injectionNames.forEach(injectionName => {
            const fn = allInjectionsMap[injectionName] || (injectionName[0] === '$' && component[injectionName]);
            if (!fn) {
                throw new Error(`Non existing dependency "${injectionName}" required by "${parentInjectionName}".`);
            }
            const statusNames = Object.keys(injectionsBeingInitialised);
            if (injectionsBeingInitialised[injectionName]) {
                throw new Error(`Dependency cycle detected: "${injectionName}" required by "${parentInjectionName}".`);
            }
            injectionsBeingInitialised[injectionName] = true;
            allInjectionsMap[injectionName] =
                fn.constructor.name === 'Function'
                    ? new fn(...this.getDependencies(fn, component, allInjectionsMap, injectionsBeingInitialised))
                    : fn;
            injectionsBeingInitialised[injectionName] = false;
        });
        return allInjectionsMap;
    }
    getDependencies(fn, component, injectionDataMap, injectionsBeingInitialised) {
        const dependencyNames = read_function_args_1.readFunctionArgs(fn);
        this.initInjections(component, dependencyNames, injectionDataMap, injectionsBeingInitialised, fn.name);
        return dependencyNames.map((dependencyName) => injectionDataMap[dependencyName]);
    }
}
exports.DiFactory = DiFactory;
