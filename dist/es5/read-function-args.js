"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function readFunctionArgs(fn) {
    var matches = (fn + '')
        .replace(/\/\*([^\*]*)\*\//g, '')
        .match(/\(([^\)]*)\)/);
    return (!matches
        ? []
        : matches[1]
            .replace(/(\s|\n|\r)*/g, '')
            .split(',')
            .filter(function (item) { return !!item; }));
}
exports.readFunctionArgs = readFunctionArgs;
