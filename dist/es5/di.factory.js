"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var read_function_args_1 = require("./read-function-args");
var DiFactory = /** @class */ (function () {
    function DiFactory(component, injectionDataMap) {
        if (injectionDataMap === void 0) { injectionDataMap = {}; }
        this.deps = this.initInjections(component, Object.keys(injectionDataMap), injectionDataMap);
    }
    DiFactory.install = function (vue, initProviders) {
        vue.mixin({
            beforeCreate: function () {
                if (!this.$di) {
                    var parentDi = this.$parent ? this.$parent.$di : null;
                    var ownProviders = this.$options.provide;
                    this.$di =
                        !parentDi
                            ? new DiFactory(this, Object.assign({}, initProviders, ownProviders || {}))
                            : (ownProviders
                                ? parentDi.copy(this, ownProviders)
                                : parentDi);
                }
            }
        });
    };
    DiFactory.prototype.copy = function (component, newDeps) {
        if (newDeps === void 0) { newDeps = {}; }
        var deps = Object.assign({}, this.deps, newDeps);
        return new DiFactory(component, deps);
    };
    DiFactory.prototype.initInjections = function (component, injectionNames, allInjectionsMap, injectionsBeingInitialised, parentInjectionName) {
        var _this = this;
        if (injectionsBeingInitialised === void 0) { injectionsBeingInitialised = {}; }
        if (parentInjectionName === void 0) { parentInjectionName = ''; }
        injectionNames.forEach(function (injectionName) {
            var fn = allInjectionsMap[injectionName] || (injectionName[0] === '$' && component[injectionName]);
            if (!fn) {
                throw new Error("Non existing dependency \"" + injectionName + "\" required by \"" + parentInjectionName + "\".");
            }
            var statusNames = Object.keys(injectionsBeingInitialised);
            if (injectionsBeingInitialised[injectionName]) {
                throw new Error("Dependency cycle detected: \"" + injectionName + "\" required by \"" + parentInjectionName + "\".");
            }
            injectionsBeingInitialised[injectionName] = true;
            allInjectionsMap[injectionName] =
                fn.constructor.name === 'Function'
                    ? new (fn.bind.apply(fn, [void 0].concat(_this.getDependencies(fn, component, allInjectionsMap, injectionsBeingInitialised))))() : fn;
            injectionsBeingInitialised[injectionName] = false;
        });
        return allInjectionsMap;
    };
    DiFactory.prototype.getDependencies = function (fn, component, injectionDataMap, injectionsBeingInitialised) {
        var dependencyNames = read_function_args_1.readFunctionArgs(fn);
        this.initInjections(component, dependencyNames, injectionDataMap, injectionsBeingInitialised, fn.name);
        return dependencyNames.map(function (dependencyName) { return injectionDataMap[dependencyName]; });
    };
    return DiFactory;
}());
exports.DiFactory = DiFactory;
