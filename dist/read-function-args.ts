export function readFunctionArgs(fn: any): string[] {
    const matches =
        (fn + '')
            .replace(/\/\*([^\*]*)\*\//g, '')
            .match(/\(([^\)]*)\)/);
    return (
        !matches
            ? []
            : matches[1]
                .replace(/(\s|\n|\r)*/g, '')
                .split(',')
                .filter((item: string) => !!item));
}
